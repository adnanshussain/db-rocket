from collections import OrderedDict
from datetime import datetime

from marshmallow_sqlalchemy import ModelSchema, ModelSchemaOpts
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy import inspect


################################################################
# Marshmallow Schemas for JSON TO / From Serialization
################################################################
def add_mm_schema(cls):
    '''
    Decorator your data model classes with this so that they can be jsonifyed
    :param cls: The Data Model Class 
    :return: 
    '''

    class Schema(ModelSchema):
        class Meta:
            model = cls
            include_fk = True

    cls.MmSchema = Schema()
    return cls


class DatabaseRocket():
    def __init__(self, db_scoped_session, sa_declarative_base):
        self.db_session = db_scoped_session
        self.sa_decl_base = sa_declarative_base

    def _get_orm_name_by_table_name(self, table_name):
        for name, cls in self.sa_decl_base._decl_class_registry.items():
            if isinstance(cls, DeclarativeMeta) and cls.__tablename__.lower() == table_name.lower():
                return cls.__name__

    def _get_orm_by_name(self, orm_name):
        # print('db-rocket: _get_orm_by_name started...')
        # print('db-rocket: ', list(self.sa_decl_base._decl_class_registry.keys()))
        for name, cls in self.sa_decl_base._decl_class_registry.items():
            # print(name, orm_name.lower(), type(cls))
            if name.lower() == orm_name.lower() and isinstance(cls, DeclarativeMeta):
                return cls

    def _get_orm_field_by_name(self, orm, field_name):
        return orm._sa_class_manager.get(field_name)

    def _get_children(self, orm, rec):
        if hasattr(orm, 'Meta') and hasattr(orm.Meta, 'children'):
            for child in orm.Meta.children:
                child_orm = child['orm']
                if isinstance(child_orm, str):
                    child_orm = self._get_orm_by_name(child_orm)

                self_fk = child['self_fk']
                if isinstance(self_fk, str):
                    self_fk = self._get_orm_field_by_name(child_orm, self_fk)

                if child['is_m2m_resolver']:
                    source_orm = child['source_orm']
                    if isinstance(source_orm, str):
                        source_orm = self._get_orm_by_name(source_orm)

                    source_fk = child['source_fk']
                    if isinstance(source_fk, str):
                        source_fk = self._get_orm_field_by_name(child_orm, source_fk)

                    # Get actual child (the m2m resolver) data
                    child_data = self.db_session.query(child_orm) \
                        .filter(self_fk == rec['id']) \
                        .all()

                    rec[child_orm.__table__.name] = [child_orm.MmSchema.dump(r).data for r in child_data]

                    # Get source child (the m2m source)
                    # child_data = self.db_session.query(source_orm) \
                    #     .join(child_orm) \
                    #     .filter(self_fk == rec['id']) \
                    #     .filter(source_fk == source_orm.id) \
                    #     .all()
                    #
                    # rec[child_orm.__table__.name + '_source'] = [source_orm.MmSchema.dump(r).data for r in child_data]
                else:
                    child_data = self.db_session.query(child_orm) \
                        .filter(self_fk == rec['id']) \
                        .all()

                    rec[child_orm.__table__.name] = [child_orm.MmSchema.dump(r).data for r in child_data]

                for child_rec in rec[child_orm.__table__.name]:
                    child_rec['_clientAction'] = None

    def _get_recursive_data(self, orm, rec, with_children):
        children = [orm.MmSchema.dump(r).data for r in self.db_session.query(orm).filter(orm.parent_id == rec['id']).all()]

        rec['children'] = children if len(children) else []

        if with_children and hasattr(orm, 'Meta') and hasattr(orm.Meta, 'children'):
            self._get_children(orm, rec)

        # Fields for Client usage and further communication
        rec['_clientAction'] = None
        rec['_filtered'] = True

        for child in children:
            self._get_recursive_data(orm, child, with_children)

    def get_by_id(self, orm_name, id, rec=None):
        orm = self._get_orm_by_name(orm_name)
        result = {} if not rec or len(rec) else rec

        if not rec:
            result = orm.MmSchema.dump(self.db_session.query(orm).filter(orm.id == id).one_or_none()).data

        if result:
            self._get_children(orm, result)

            # Fields for Client usage and further communication
            result['_clientAction'] = None
            result['_filtered'] = True

        return result

    def get_all(self, orm_name, with_children=False, page_size=None, page_no=None, ids=None, full_data=False,
                search_in=None, search_text=None,
                order_by=None, order_by_dir='asc',
                limit=None):
        orm = self._get_orm_by_name(orm_name)
        result = []

        query = self.db_session.query(orm)

        ordering_field = orm.id
        if order_by:
            ordering_field = self._get_orm_field_by_name(orm, order_by)
            # query = query.order_by(self._get_orm_field_by_name(orm, order_by))
        # else:
        #     query = query.order_by(orm.id)

        if order_by_dir == 'asc':
            query = query.order_by(ordering_field)
        else:
            query = query.order_by(ordering_field.desc())

        if page_size and page_no:
            offset = (page_no - 1) * page_size
            query = query.offset(offset)
            query = query.limit(page_size)

        if ids:
            ids_to_query = list(set([int(x) for x in ids.split(',')]))
            query = query.filter(orm.id.in_(ids_to_query))

        if search_in:
            search_fields = [self._get_orm_field_by_name(orm, r) for r in search_in.split(',')]
            search_texts = [r for r in search_text.split(',')]

            search_criterias = zip(search_fields, search_texts)

            for sf, st in search_criterias:
                query = query.filter(sf.like('%' + st + '%'))

        if limit:
            query = query.limit(limit)

        if hasattr(orm, 'Meta') and hasattr(orm.Meta, 'is_recursive'):
            query = query.filter(orm.parent_id == None)

            result = [orm.MmSchema.dump(r).data if full_data else {'id': orm.MmSchema.dump(r).data['id'], 'name': orm.MmSchema.dump(r).data.get('name')} for r in query.all()]

            for result_rec in result:
                self._get_recursive_data(orm, result_rec, with_children)
        else:
            result = [orm.MmSchema.dump(r).data if full_data else {'id': orm.MmSchema.dump(r).data['id'], 'name': orm.MmSchema.dump(r).data.get('name')} for r in query.all()]

        if with_children and hasattr(orm, 'Meta') and hasattr(orm.Meta, 'children'):
            for result_rec in result:
                self._get_children(orm, result_rec)

        for result_rec in result:
            # Fields for Client usage and further communication
            result_rec['_clientAction'] = None
            result_rec['_filtered'] = True

        return result

    def save(self, orm, rec_to_save):
        rec = None

        clientAction = rec_to_save.get('_clientAction')

        if clientAction == 'added' or clientAction == 'updated':
            if clientAction == 'added':
                rec = orm()

                if hasattr(rec, 'created_on'):
                    setattr(rec, 'created_on', datetime.now())
            else:
                query = self.db_session.query(orm)

                for pkCol in orm.__table__.primary_key.columns:
                    query = query.filter(self._get_orm_field_by_name(orm, pkCol.name) == rec_to_save[pkCol.name])

                rec = query.one()

            for k, v in rec_to_save.items():
                # TODO: id check should only be for Auto ID tables
                if not isinstance(v, list) \
                        and not k.startswith('_') \
                        and k not in ['created_on'] \
                        and k != 'id':
                    if isinstance(v, str):
                        setattr(rec, k, str(v).strip())
                    setattr(rec, k, v)

            if clientAction == 'added':
                self.db_session.add(rec)
                self.db_session.flush()

            if hasattr(orm, 'Meta') and hasattr(orm.Meta, 'children'):
                for child in orm.Meta.children:
                    if child['is_m2m_resolver']:
                        child_orm = child['orm']
                        if isinstance(child_orm, str):
                            child_orm = self._get_orm_by_name(child_orm)
                        self_fk = child['self_fk']
                        if isinstance(self_fk, str):
                            self_fk = self._get_orm_field_by_name(child_orm, self_fk)

                        source_fk = None
                        if child.get('source_fk'):
                            source_fk = child['source_fk']
                            if isinstance(source_fk, str):
                                source_fk = self._get_orm_field_by_name(child_orm, source_fk)

                        if rec_to_save.get(child_orm.__tablename__) != None:
                            # Delete all child records
                            self.db_session.query(child_orm).filter(self_fk == rec.id).delete()

                            # For each child record to save
                            for child_rec_to_save in rec_to_save[child_orm.__tablename__]:
                                child_rec = child_orm()
                                setattr(child_rec, self_fk.key, rec.id)
                                setattr(child_rec, source_fk.key, child_rec_to_save[source_fk.key])

                                # Any additional columns being sent from the client to be saved
                                for k, v in child_rec_to_save.items():
                                    if k not in [self_fk.key, source_fk.key]:
                                        # TODO: Check if the db model has the field trying to be set, if not then raise an error
                                        setattr(child_rec, k, v)

                                self.db_session.add(child_rec)
        elif clientAction == 'deleted':
            query = self.db_session.query(orm)

            for pkCol in orm.__table__.primary_key.columns:
                query = query.filter(self._get_orm_field_by_name(orm, pkCol.name) == rec_to_save[pkCol.name])

            rec = query.delete()

        self.db_session.commit()

        return rec

    def get_orm_structure(self, orm_name):
        orm = self._get_orm_by_name(orm_name)
        orm_inspector = inspect(orm)

        result = {}

        for c in orm_inspector.columns:
            result[c.key] = {
                'is_pk': c.primary_key,
                'type': str(c.type),
                'is_fk': True if c.foreign_keys else False,
                'fk_source': [self._get_orm_name_by_table_name(fk.column.table.name) for fk in c.foreign_keys][0] if c.foreign_keys else None
            }

        return result
