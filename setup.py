# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
# with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
#     long_description = f.read()

setup(
    name='db_rocket',
    version='0.0.1',

    # Author details
    author='Adnan Hussain',
    author_email='adnanshussain@gmail.com',

    # Choose your license
    license='MIT',

    packages=['db_rocket']
)
